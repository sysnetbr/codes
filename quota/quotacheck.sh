#!/usr/bin/env bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin

# Define variáveis para o script.
init () {
    # Caminho para o arquivo de fila
    QUEUE_FILE="/tmp/queue.txt"
    LOG_FILE="/var/log/quota.log"

    destinations="emails"

    # Tempo de espera em segundos (4 horas = 4 * 60 * 60)
    WAIT_TIME=14400

    WARNING=80
    CRITICAL=99

    send_notification_critical_BODY="The following users/groups have over their quota:"
    send_notification_critical_SUBJECT="User or Group is Over quota"

    # Verifica se o arquivo de fila existe, caso contrário, cria um novo
    if [ ! -f "$QUEUE_FILE" ]; then
      touch "$QUEUE_FILE"
    fi

    if [ ! -f "$LOG_FILE" ]; then
      touch "$LOG_FILE"
    fi
}

# Notifica sobre o consumo de quota para quando o usuário já estourou a quota, se ele já foi notificado não será notificado até que tenha dado o tempo em WAIT_TIME.
send_notification_critical () {
    # Executa o comando repquota para obter a lista de usuários com a quota expirada
    for userQuota in $(/usr/sbin/repquota -us /stg_nfs | awk '$2=="+-" {print $1}')
    do
        if [ $(grep -o "${userQuota}" "${QUEUE_FILE}") ]; then
            userTimestamp=$(grep "${userQuota}" "${QUEUE_FILE}" | awk '{ print $1 }')
            currentTimestamp=$(date +%s)
            diffSeconds=$((currentTimestamp - userTimestamp))

            if [ ${diffSeconds} -ge ${WAIT_TIME} ];then
                sed -i "/${userQuota}/d" "${QUEUE_FILE}"
                echo "$(date +%s) - ${userQuota}" >> "$QUEUE_FILE"
                echo "$(date +%s) - ${userQuota}" >> "$LOG_FILE"

                echo "${send_notification_critical_BODY} ${userQuota}" | /usr/bin/mail -aFrom:quota@test -s "${send_notification_critical_SUBJECT}" ${destinations}
                echo "${send_notification_critical_BODY} ${userQuota}" >> "$LOG_FILE"
            fi
        
        else
            echo "$(date +%s) - ${userQuota}" >> "$QUEUE_FILE"
            echo "$(date +%s) - ${userQuota}" >> "$LOG_FILE"
            echo "${send_notification_critical_BODY} ${userQuota}" | /usr/bin/mail -aFrom:quota@test -s "${send_notification_critical_SUBJECT}" ${destinations}
            echo "${send_notification_critical_BODY} ${userQuota}" >> "$LOG_FILE"
        fi
    done
}

# Notifica sobre o consumo de quota, o threshold é definido por CRITICAL e WARNING.
check_threshold () {
    IFS=$'\n'
    for line in $(/usr/sbin/repquota -us /stg_nfs | sed '1,5d')
    do
        # Se o soft não tiver quota ele pula a verificação do usuário.
        if [ $(awk '{ print $4 }' <<<${line}) == 0K ]; then
            continue
        fi

        username=$(awk '{ print $1 }' <<<${line})

        # Se o usuário já está na fila, pula a verificação também.
        if [ $(grep -o "${username}" "${QUEUE_FILE}") ]; then
            continue
        
        # Se não está na fila verifica o consumo para talvez enviar uma notificação.
        else
            usedStorageInK_M=$(awk '{ print $3 }' <<<${line})
            usedStorage="${usedStorageInK_M%[MK]*}"
            
            warningStorageInK_M=$(awk '{ print $4 }' <<<${line})
            warningStorage="${warningStorageInK_M%[MK]*}"

            percentage=$(echo "scale=4; ((${usedStorage}/${warningStorage})*100)" | /usr/bin/bc | cut -d '.' -f 1)
            percentage=${per:-"0"}

            if [ ${percentage} -ge ${WARNING} ] && [ ${percentage} -le ${CRITICAL} ]; then
                echo "$(date +%s) - ${username}" >> "$QUEUE_FILE"
                echo "$(date +%s) - ${username}" >> "$LOG_FILE"

                echo "The following users have their quota at ${percentage}%: ${username}" | /usr/bin/mail -aFrom:quota@test -s "User or Group is Over quota" ${destinations}
                echo "The following users have their quota at ${percentage}%: ${username}" >> "$LOG_FILE"
            fi
        fi
    done
}

# Remove os usuários da fila depois de WAIT_TIME após terem sido notificados.
remove_user_from_queue () {

    if [ -s "$QUEUE_FILE" ]; then
        IFS=$'\n'
        for queueUser in $(cat "${QUEUE_FILE}"); 
        do
    
            usernameInQueue=$(awk '{ print $3 }' <<< ${queueUser})
            userQueueTimestamp="$(awk '{ print $1 }' <<< ${queueUser})"
            currentQueueTimestamp=$(date +%s)
            diffQueueSeconds=$((currentQueueTimestamp - userQueueTimestamp))

            if [ ${diffQueueSeconds} -ge ${WAIT_TIME} ];then
                echo "${currentQueueTimestamp} - Removing ${usernameInQueue} from Queue." >> "$LOG_FILE"
                sed -i "/${usernameInQueue}/d" "${QUEUE_FILE}"
            fi
        done
    fi
}

init
remove_user_from_queue
send_notification_critical
check_threshold

# Converter timestamp para data/hora:
## date -d @$timestamp

# Converter data/hora em timestamp:
## date +%s

