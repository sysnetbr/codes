#!/bin/bash 

sudo apt update
sudo apt install -y net-tools isc-dhcp-server

sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/' /etc/sysctl.conf
sysctl -p /etc/sysctl.conf 

sudo echo -e "[Service]\nType=oneshot\nRemainAfterExit=yes\nExecStart=iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE\nExecStop=iptables -t nat -F\n\n[Install]\nWantedBy=multi-user.target" | sudo tee /etc/systemd/system/my-startup-firewall.service
sudo systemctl enable my-startup-firewall.service
sudo systemctl start my-startup-firewall.service

sudo sed -i "s/INTERFACESv4=.*/INTERFACESv4=\"eth1 eth2\"/" /etc/default/isc-dhcp-server

sudo echo -e "default-lease-time 600;
max-lease-time 7200;
authoritative;
ignore client-updates;
log-facility local7;

subnet 192.168.151.0 netmask 255.255.255.0
{
   range 192.168.151.15 192.168.151.250;
   option routers 192.168.151.2;
   option domain-name-servers 8.8.8.8, 8.8.4.4;
}

subnet 192.168.200.0 netmask 255.255.255.0
{
   range 192.168.200.15 192.168.200.250;
   option routers 192.168.200.2;
   option domain-name-servers 8.8.8.8, 8.8.4.4;
}" > /etc/dhcp/dhcpd.conf

sudo systemctl restart isc-dhcp-server